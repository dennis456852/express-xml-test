var express = require('express');
var router = express.Router();
var xml = require('xml')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/receive-xml', function (req, res, next) {

  console.log("req.body = ", req.body)
  console.log("req.body.note = ", req.body.note)
  console.log("req.body.note.title = ", req.body.note.title)
  console.log("req.body.note.from = ", req.body.note.from)
  console.log("req.body.note.to = ", req.body.note.to)

  res.set('Content-Type', 'application/xml');
  res.send(xml({ name: "邱天" }));

});
module.exports = router;
